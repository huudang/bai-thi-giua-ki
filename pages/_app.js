// import "../styles/globals.css";
import "antd/dist/antd.css";
import { Table } from "antd";
import { useEffect } from "react";

const MyApp = ({ Component, pageProps }) => {
  // useEffect(() => {
  //   localStorage.setItem("projects", "[]");
  //   console.log(localStorage);
  // });
  return <Component {...pageProps} />;
};

export default MyApp;
