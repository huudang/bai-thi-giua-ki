import { routerLink, routerUtils } from "../router";

const HomePage = () => {
  return null;
};

HomePage.getInitialProps = async (ctx) => {
  routerUtils.redirect(ctx, routerLink.register.index.get().linkProps());

  return {
    namespacesRequired: ["global"],
  };
};

export default HomePage;
