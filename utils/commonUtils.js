export const sleep = (ms = 1000) => {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      resolve()
    }, ms)
  })
}

export const numberFormatter = (number) => number?.toString()?.replace(/\B(?=(\d{3})+(?!\d))/g, '.') ?? null

export const currencyFormatter = (total) => numberFormatter(total.price) + `${total.currency}`

export const phoneFormatter = (phone) => {
  phone = `${phone}`

  if (!phone) {
    return ''
  }

  if (phone.charAt(0) === '0') {
    // 0773651408 -> 773651408
    phone = phone.substr(1)
  } else if (phone.charAt(0) === '8' && phone.charAt(1) === '4') {
    // 84773651408 -> 773651408
    phone = phone.substr(2)
  } else if (phone.charAt(0) === '+') {
    // +84773651408 -> 773651408
    phone = phone.substr(3)
  }

  return '0' + phone
}

const commonUtils = {
  sleep,
  numberFormatter,
  currencyFormatter,
  phoneFormatter
}

export default commonUtils
