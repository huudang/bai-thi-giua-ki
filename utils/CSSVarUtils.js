const CSSVarUtils = {
  setProperty: (name, value) => {
    document.documentElement.style.setProperty(`--${name}`, value)
  },
  getProperty: (name) => {
    return `var(--${name})`
  }
}

export default CSSVarUtils
