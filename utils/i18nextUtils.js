const NextI18Next = require('next-i18next').default

const NextI18NextInstance = new NextI18Next({
  defaultLanguage: 'vi',
  otherLanguages: ['en'],
  defaultNS: 'global',
  localePath: 'locales'
})

module.exports = NextI18NextInstance
