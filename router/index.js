const createRouter = require("./createRouter");
const routerUtils = require("./routerUtils");

const router = {
  home: {
    index: {
      href: "/",
      as: "/",
      file: "/",
    },
  },
  list: {
    index: {
      href: "/list",
      as: "/list",
      file: "/list",
    },
  },
  register: {
    index: {
      href: "/register",
      as: "/register",
      file: "/register",
    },
  },
};

module.exports.router = router;
module.exports.routerLink = createRouter(router);
module.exports.routerUtils = routerUtils;
