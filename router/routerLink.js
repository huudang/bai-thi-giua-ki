import createRouter from './createRouter'
import router from './index'

const routerLink = createRouter(router)

export default routerLink
