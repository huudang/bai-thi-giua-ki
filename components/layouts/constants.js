import { routerLink } from "../../router";

export const menuList = [
  {
    key: "register",
    name: "Thêm nhóm",
    isDisable: false,
    link: routerLink.register.index.get().linkProps(),
  },
  {
    key: "list",
    name: "Danh sách nhóm",
    isDisable: true,
    link: routerLink.list.index.get().linkProps(),
  },
];
