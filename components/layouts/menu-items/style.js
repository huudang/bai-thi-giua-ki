import styled from "styled-components";
import themeConstants from "../../../theme/constants";

export const S4MenuItem = styled.a`
  display: flex;
  align-items: center;
  flex-wrap: nowrap;
  margin-left: 8px;
  margin-right: 8px;
  padding: 8px;
  border-radius: ${themeConstants.borderRadius.md};
  transition: ${themeConstants.transition["0.24"]};

  &,
  &:hover {
    color: ${themeConstants.text.darkColor};
  }

  &:hover {
    background: ${themeConstants.color.gray[100]};
  }

  &.is-active {
    background: ${themeConstants.color.primaryTransparent[100]};
    color: ${themeConstants.text.primaryColor};
    font-weight: ${themeConstants.fontWeights.medium};
  }
`;

export const S4Icon = styled.span`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 32px;
  height: 32px;
  border-radius: 16px;
`;

export const S4NameWrapper = styled.span`
  position: relative;
  left: 16px;
  width: calc(100% - 48px);
  white-space: nowrap;
`;

export const S4Name = styled.span`
  display: flex;
`;

export const S4Disable = styled.a`
  pointer-events: none;
`;
