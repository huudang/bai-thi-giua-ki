import { Tooltip } from "antd";

import Link from "next/link";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";
import classNames from "classnames";
import { Typography } from "antd";

const { Text } = Typography;

import { S4MenuItem, S4NameWrapper, S4Name, S4Disable } from "./style";

const MenuItem = ({
  link = {
    href: "/",
    as: "/",
  },

  name,
  tooltip,
  isActive = false,
}) => {
  const [disable, setDisable] = useState(false);

  useEffect(() => {
    if (!localStorage.projects && name === "Danh sách nhóm") setDisable(true);
    console.log(localStorage);
  }, []);

  let children;
  if (disable) {
    children = (
      <S4Disable>
        <Link {...link} passHref>
          <Tooltip title={tooltip} placement="right">
            <S4MenuItem className={classNames({ "is-active": isActive })}>
              <S4NameWrapper>
                <S4Name>{name}</S4Name>
              </S4NameWrapper>
            </S4MenuItem>
          </Tooltip>
        </Link>
      </S4Disable>
    );
  } else {
    children = (
      <Link {...link} passHref>
        <Tooltip title={tooltip} placement="right">
          <S4MenuItem className={classNames({ "is-active": isActive })}>
            <S4NameWrapper>
              <S4Name>{name}</S4Name>
            </S4NameWrapper>
          </S4MenuItem>
        </Tooltip>
      </Link>
    );
  }

  return children;
};

MenuItem.propTypes = {
  link: PropTypes.shape({
    href: PropTypes.string,
    as: PropTypes.string,
  }),

  isImageIcon: PropTypes.bool,
  imageIcon: PropTypes.element,

  iconName: PropTypes.string,
  iconSize: PropTypes.any,
  iconColor: PropTypes.any,

  name: PropTypes.oneOfType([PropTypes.string, PropTypes.element]).isRequired,
  tooltip: PropTypes.string,
  isActive: PropTypes.bool,
};

export default MenuItem;
