import MenuItem from "../menu-items";
import PropTypes from "prop-types";
import React, { useEffect, useState } from "react";

const MenuList = ({ menuList, isCollapse, activeKey }) => {
  return menuList.map((item) => {
    return (
      <MenuItem
        key={item.key}
        name={item.name}
        tooltip={isCollapse ? item.name : null}
        link={item.link}
        isActive={item.key === activeKey}
      />
    );
  });
};

MenuList.propTypes = {
  menuList: PropTypes.array,
  isCollapse: PropTypes.bool,
  activeKey: PropTypes.string,
};

export default MenuList;
