import HeaderMeta from "./header";
import { Layout } from "antd";
import styled from "styled-components";
import themeConstants from "../../theme/constants";
import classnames from "classnames";
import MenuList from "./menu-list";
import Clearfix from "../clearfix";

import { menuList } from "./constants";

const SIDEBAR_DEFAULT_WIDTH = "240px";
const SIDEBAR_COLLAPSED_WIDTH = "64px";

const MAIN_DEFAULT_WIDTH = `calc(100vw - ${SIDEBAR_DEFAULT_WIDTH})`;
const MAIN_COLLAPSED_WIDTH = `calc(100vw - ${SIDEBAR_COLLAPSED_WIDTH})`;

export const S4Sidebar = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  z-index: ${themeConstants.zIndex.sidebar};

  display: flex;
  flex-direction: column;
  overflow-x: hidden;
  overflow-y: auto;

  background: #fff;
  transition: ${themeConstants.transition["0.24"]};
  box-shadow: ${themeConstants.shadow.md};
`;

const S4Layout = styled.div`
  .sidebar {
    width: ${SIDEBAR_DEFAULT_WIDTH};
    height: 100vh;
  }

  .main {
    left: ${SIDEBAR_DEFAULT_WIDTH};
  }

  .main,
  .header {
    width: ${MAIN_DEFAULT_WIDTH};
  }

  &.is-collapse {
    .sidebar {
      width: ${SIDEBAR_COLLAPSED_WIDTH};
    }

    .main {
      left: ${SIDEBAR_COLLAPSED_WIDTH};
    }

    .main,
    .header {
      width: ${MAIN_COLLAPSED_WIDTH};
    }
  }
`;

export const S4Main = styled.div`
  position: relative;
  padding-top: 32px;
  transition: ${themeConstants.transition["0.24"]};
`;

const S4NameWrapper = styled.span`
  position: relative;
  left: 16px;
  width: calc(100% - 48px);
  white-space: nowrap;
`;

const S4Name = styled.span`
  display: flex;
`;

export const S4MenuItem = styled.a`
  display: flex;
  align-items: center;
  flex-wrap: nowrap;
  margin-left: 8px;
  margin-right: 8px;
  padding: 8px;
  border-radius: ${themeConstants.borderRadius.md};
  transition: ${themeConstants.transition["0.24"]};

  &,
  &:hover {
    color: ${themeConstants.text.darkColor};
  }

  &:hover {
    background: ${themeConstants.color.gray[100]};
  }

  &.is-active {
    background: ${themeConstants.color.primaryTransparent[100]};
    color: ${themeConstants.text.primaryColor};
    font-weight: ${themeConstants.fontWeights.medium};
  }
`;

export const S4Icon = styled.span`
  display: flex;
  align-items: center;
  justify-content: center;

  width: 32px;
  height: 32px;
  border-radius: 16px;
`;

const MainLayout = ({ children, title, pageKey, currentTab }) => {
  return (
    <>
      <HeaderMeta title={title} />
      <Layout className="layout">
        <S4Layout
          className={classnames({
            "is-collapse": false,
          })}
        >
          <S4Sidebar className="sidebar">
            <Clearfix height={48} />

            <MenuList
              menuList={menuList}
              activeKey={pageKey}
              //   isCollapse={isCollapse}
            />

            <div style={{ flexGrow: 1 }} />
          </S4Sidebar>

          <S4Main className="main">
            <div>{children}</div>
          </S4Main>
        </S4Layout>
      </Layout>
    </>
  );
};

export default MainLayout;
