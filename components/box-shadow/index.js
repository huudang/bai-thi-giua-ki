import styled from 'styled-components'
import themeConstants from '../../theme/constants'

const S4BoxShadow = styled.div`
  padding: ${props => `${props.paddingY}px ${props.paddingX}px`};
  background: #fff;
  box-shadow: ${props => props.boxShadow};
  border-radius: ${props => props.borderRadius};
  overflow: hidden;
`

const BoxShadow = ({
  children,
  paddingX = 24,
  paddingY = 16,
  boxShadow = themeConstants.shadow.md,
  borderRadius = themeConstants.borderRadius.md,
  ...otherProps
}) => {
  return (
    <S4BoxShadow
      paddingX={paddingX}
      paddingY={paddingY}
      boxShadow={boxShadow}
      borderRadius={borderRadius}
      {...otherProps}
    >
      {children}
    </S4BoxShadow>
  )
}

export default BoxShadow
