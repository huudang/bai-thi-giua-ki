import defaultColors from "../colors";

const colors = {
  black: defaultColors.black,
  white: defaultColors.white,
  gray: defaultColors.gray[400],

  primary: defaultColors.primary[500],
  success: defaultColors.success[400],
  warning: defaultColors.warning[400],
  danger: defaultColors.danger[400],

  border: defaultColors.gray[200],
};

const themeConstants = {
  colors,
  color: defaultColors,

  transition: {
    0.24: "all 0.24s ease-in-out",
  },
  border: {
    defaultColor: colors.border,
    primaryColor: colors.primary,
  },
  text: {
    lightColor: colors.gray,
    darkColor: colors.black,

    primaryColor: colors.primary,
    successColor: colors.success,
    warningColor: colors.warning,
    dangerColor: colors.danger,
  },

  fontWeights: {
    regular: "400",
    medium: "600",
    bold: "700",
  },

  fontSize: {
    sm: "12px",
    md: "14px",
    lg: "18px",
    xl: "20px",
  },

  borderRadius: {
    sm: "2px",
    md: "4px",
    lg: "8px",
    full: "999px",
  },

  shadow: {
    sm: "0 1px 3px 0 rgba(0, 0, 0, 0.1), 0 1px 2px 0 rgba(0, 0, 0, 0.06)",
    md: "0 4px 6px -1px rgba(0, 0, 0, 0.1), 0 2px 4px -1px rgba(0, 0, 0, 0.06)",
    lg: "0 10px 15px -3px rgba(0, 0, 0, 0.1), 0 4px 6px -2px rgba(0, 0, 0, 0.05)",
    xl: "0 20px 25px -5px rgba(0, 0, 0, 0.1), 0 10px 10px -5px rgba(0, 0, 0, 0.04)",
    "2xl": "0 25px 50px -12px rgba(0, 0, 0, 0.25)",
    outline: "0 0 0 3px rgba(66, 153, 225, 0.6)",
    inner: "inset 0 2px 4px 0 rgba(0,0,0,0.06)",
    none: "none",
  },

  zIndex: {
    hide: -1,
    auto: "auto",
    badge: "auto",
    tableFixed: 2,
    affix: 10,
    backTop: 10,
    pickerPanel: 10,
    popupClose: 10,
    modal: 1000,
    modalMask: 1000,
    message: 1010,
    notification: 1010,
    popover: 1030,
    dropdown: 1050,
    picker: 1050,
    popoconfirm: 1060,
    tooltip: 1070,
    image: 1080,

    header: 900,
    sidebar: 901,
  },
};

export default themeConstants;
