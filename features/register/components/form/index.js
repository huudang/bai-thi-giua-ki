import React, { useEffect, useState } from "react";
import {
  Form,
  Input,
  Button,
  Divider,
  Radio,
  Space,
  Tooltip,
  message,
} from "antd";
import Clearfix from "../../../../components/clearfix";
import { routerUtils, routerLink } from "../../../../router";

import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";

var data = [];

const FormRegister = () => {
  const [result, setResult] = useState();
  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    data =
      JSON.parse(localStorage.getItem("projects")) != 0 &&
      JSON.parse(localStorage.getItem("projects"))
        ? JSON.parse(localStorage.getItem("projects"))
        : [];
  }, []);

  useEffect(async () => {
    const pushData = async () => {
      data.push(result);

      localStorage.projects = JSON.stringify(data);
      //   data = JSON.parse(localStorage.projects) | [];
    };
    if (result) {
      await pushData();
    }
  }, [result]);

  const onFinish = (values) => {
    setIsLoading(true);
    const newResult = {
      ...values.project,
      lead: values.project.mssvLead + " - " + values.project.fullnameLead,
      member: values.project.mssvLead + "\n",
      //   index: localStorage.index ? localStorage.index + 1 : 1,
      index:
        localStorage && localStorage.projects
          ? JSON.parse(localStorage.projects).length + 1
          : 1,
    };
    values.users &&
      values.users.map((user) => {
        newResult.member += user.mssv + "\n";
      });
    setResult(newResult);
    setTimeout(() => {
      message.success("Đăng ký nhóm thành công");
      const tmp = parseInt(localStorage.index);
      console.log(localStorage.index);

      console.log(tmp);
      tmp && tmp != NaN
        ? (localStorage.index += parseInt(tmp) + 1)
        : (localStorage.index = parseInt(1));
      setIsLoading(false);
      return routerUtils.redirect(
        "/list",
        routerLink.list.index.get().linkProps()
      );
    }, 500);
  };

  return (
    <>
      <Form
        labelCol={{ span: 4 }}
        wrapperCol={{ span: 14 }}
        layout="horizontal"
        size="large"
        initialValues={{
          project: {
            name: "",
            type: "App",
            desc: "",
            member: [],
          },
        }}
        onFinish={onFinish}
      >
        <Form.Item
          wrapperCol={{
            xs: {
              span: 24,
              offset: 0,
            },
            sm: {
              span: 16,
              offset: 4,
            },
          }}
        >
          <Divider>ĐĂNG KÝ ĐỒ ÁN NHÓM</Divider>
        </Form.Item>

        <Clearfix height={32} />

        <Form.Item
          name={["project", "name"]}
          label="Tên đồ án"
          rules={[{ required: true, message: "Bắt buộc phải có Tên đồ án" }]}
          wrapperCol={{
            xs: {
              span: 24,
              offset: 0,
            },
            sm: {
              span: 16,
              offset: 2,
            },
          }}
        >
          <Input />
        </Form.Item>

        <Divider />

        <Form.Item
          name={["project", "type"]}
          label="Loại đồ án"
          rules={[{ required: false, message: "Hãy chọn loại đồ án" }]}
          wrapperCol={{
            xs: {
              span: 24,
              offset: 0,
            },
            sm: {
              span: 16,
              offset: 2,
            },
          }}
        >
          <Radio.Group>
            <Radio value="App">App</Radio>
            <Radio value="Web">Web</Radio>
          </Radio.Group>
        </Form.Item>

        <Divider />

        <Form.Item
          name={["project", "desc"]}
          label="Introduction"
          wrapperCol={{
            xs: {
              span: 24,
              offset: 0,
            },
            sm: {
              span: 16,
              offset: 2,
            },
          }}
        >
          <Input.TextArea />
        </Form.Item>

        <Divider />

        <Form.Item
          name={["project", "member"]}
          label="Thành viên"
          wrapperCol={{
            xs: {
              span: 24,
              offset: 0,
            },
            sm: {
              span: 16,
              offset: 2,
            },
          }}
          rules={[{ required: false, message: "Phải có ít nhất 1 thành viên" }]}
        >
          <Space style={{ display: "flex", marginBottom: 8 }} align="baseline">
            <Tooltip title="Nhóm trưởng">
              <Form.Item
                name={["project", "mssvLead"]}
                rules={[{ required: true, message: "Bắt buộc phải có MSSV" }]}
              >
                <Input placeholder="MSSV" />
              </Form.Item>
            </Tooltip>
            <Form.Item
              name={["project", "fullnameLead"]}
              rules={[
                { required: true, message: "Bắt buộc phải có Họ và tên" },
              ]}
            >
              <Input placeholder="Họ và Tên" />
            </Form.Item>
            <MinusCircleOutlined onClick={() => remove(name)} />
          </Space>{" "}
          <Form.List name="users">
            {(fields, { add, remove }) => (
              <>
                {fields.map(({ key, name, fieldKey, ...restField }) => (
                  <Space
                    key={key}
                    style={{ display: "flex", marginBottom: 8 }}
                    align="baseline"
                  >
                    <Form.Item
                      {...restField}
                      name={[name, "mssv"]}
                      fieldKey={[fieldKey, "mssv"]}
                      rules={[
                        { required: true, message: "Bắt buộc phải có MSSV" },
                      ]}
                    >
                      <Input placeholder="MSSV" />
                    </Form.Item>
                    <Form.Item
                      {...restField}
                      name={[name, "fullname"]}
                      fieldKey={[fieldKey, "fullname"]}
                      rules={[
                        {
                          required: true,
                          message: "Bắt buộc phải có Họ và Tên",
                        },
                      ]}
                    >
                      <Input placeholder="Họ và Tên" />
                    </Form.Item>
                    <MinusCircleOutlined onClick={() => remove(name)} />
                  </Space>
                ))}
                <Form.Item>
                  <Button
                    type="dashed"
                    onClick={() => add()}
                    block
                    icon={<PlusOutlined />}
                  >
                    Thêm thành viên
                  </Button>
                </Form.Item>
              </>
            )}
          </Form.List>
        </Form.Item>
        {/* <Form.Item>
          <Button type="primary" htmlType="submit">
            Dang ki
          </Button>
        </Form.Item> */}

        <Divider />

        <Form.Item
          wrapperCol={{
            xs: {
              span: 24,
              offset: 0,
            },
            sm: {
              span: 16,
              offset: 11,
            },
          }}
        >
          <Button
            type="primary"
            htmlType="submit"
            style={{ borderRadius: "8px" }}
            loading={isLoading}
          >
            Đăng ký
          </Button>
        </Form.Item>
      </Form>
    </>
  );
};

export default FormRegister;
