import React from "react";

import MainLayout from "../../../../components/layouts";

const HomeLayout = ({ children }) => {
  return (
    <MainLayout title="Thêm nhóm" pageKey="register">
      {children}
    </MainLayout>
  );
};

export default HomeLayout;
