import React from "react";
import RegisterLayout from "../components/layout";
import { Container } from "styled-bootstrap-grid";
import BoxShadow from "../../../components/box-shadow";
import Form from "../components/form";

const RegisterFeature = () => {
  return (
    <RegisterLayout>
      <Container fluid>
        <BoxShadow style={{ minHeight: "calc(100vh - var(--header-height))" }}>
          <Form />
        </BoxShadow>
      </Container>
    </RegisterLayout>
  );
};

export default RegisterFeature;
