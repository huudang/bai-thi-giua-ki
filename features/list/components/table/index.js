import React, { useEffect, useState } from "react";

import { Table, Tooltip, Typography } from "antd";

const { Text } = Typography;

const EllipsisMiddle = ({ suffixCount, children }) => {
  const split = children.split(" ");
  let result = "";
  for (let i = 0; i < 10; i++) {
    if (i >= split.length) {
      break;
    }
    result += " " + split[i];
  }
  if (split.length >= 10) result += "...";

  // const start = children.slice(0, children.length - suffixCount).trim();
  const suffix = children.slice(-suffixCount).trim();
  return <Text style={{ maxWidth: "100%" }}>{result.trim()}</Text>;
};

const columns = [
  {
    title: "#",
    dataIndex: "index",
    key: "index",
    width: 56,
  },
  {
    title: "Tên đồ án",
    dataIndex: "name",
    key: "name",
    width: 320,
  },
  {
    title: "Loại đồ án",
    dataIndex: "type",
    key: "type",
    width: 106,
  },
  {
    title: "Nhóm trưởng",
    dataIndex: "lead",
    key: "lead",
  },
  {
    title: "Danh sách thành viên",
    dataIndex: "member",
    key: "member",
    width: 180,
  },
  {
    title: "Mô tả",
    dataIndex: "desc",
    key: "desc",
    // ellipsis: {
    //   showTitle: false,
    // },
    render: (desc) => {
      return <EllipsisMiddle suffixCount={9}>{desc}</EllipsisMiddle>;
    },
  },
];

const TableResult = () => {
  const [dataSource, setDataSource] = useState([]);

  useEffect(() => {
    setDataSource(JSON.parse(localStorage.getItem("projects")));
  }, []);

  return (
    <>
      <Table
        columns={columns}
        dataSource={dataSource}
        // components={components}
        rowClassName={() => "editable-row"}
        sticky
        pagination={{
          position: ["bottomCenter"],
          style: {
            paddingRight: "1rem",
          },
          showSizeChanger: true,
        }}
      />
    </>
  );
};

export default TableResult;
