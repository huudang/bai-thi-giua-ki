import React from "react";

import MainLayout from "../../../../components/layouts";
import { Layout } from "antd";

const { Content } = Layout;

const HomeLayout = ({ children }) => {
  return (
    <MainLayout title="Danh sách đăng ký" pageKey="list">
      {children}
    </MainLayout>
  );
};

export default HomeLayout;
