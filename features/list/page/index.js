import React from "react";
import ListLayout from "../components/layout";
import TableResult from "../components/table";
import { Container } from "styled-bootstrap-grid";
import BoxShadow from "../../../components/box-shadow";

const List = () => {
  return (
    <ListLayout>
      <Container fluid>
        <BoxShadow style={{ minHeight: "calc(100vh - var(--header-height))" }}>
          <TableResult />
        </BoxShadow>
      </Container>
    </ListLayout>
  );
};

export default List;
